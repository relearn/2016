/* important: change font to monospace */

#!/usr/bin/non-python
# -*- coding: cthulhu-1 -*-

from relearn import relearn.without.time
path = "/calafou/casaverde"



╔═║╔═╝║  ╔═╝╔═║╔═║╔═   ══║╔═║═║ ╔═╝
╔╔╝╔═╝║  ╔═╝╔═║╔╔╝║ ║  ╔═╝║╝║ ║ ╔═║
╝ ╝══╝══╝══╝╝ ╝╝ ╝╝ ╝  ══╝══╝══╝══╝
╔═║╔═ ╔═╝═╔╝╔═║║ ║╔═╝═╔╝╝╔═║╔═   ╔╔ ╔═║╔═ ║ ║╔═║║  
║ ║╔═║══║ ║ ╔╔╝║ ║║   ║ ║║ ║║ ║  ║║║╔═║║ ║║ ║╔═║║  
══╝══ ══╝ ╝ ╝ ╝══╝══╝ ╝ ╝══╝╝ ╝  ╝╝╝╝ ╝╝ ╝══╝╝ ╝══╝

Dear reader or archive,
dear human and non-human agents,
whereever this text goes, 
    * magnetised on a metall circle or maybe a laser disc
    * a piece of paper in a bottle swimming in a pollutted river in spain catalunya
    * silk screen sieve 
    * a assemblage  of networks devices and glass fiber, spinned around planets
    * into the thoughts of a living thing
    * an electronic paper that flies in the personfication of the upper sky
    * ...
, can't be determined by the writers of this *what ever it is / the thing that we don't know yet*. 
Purpose of *what ever it is / the thing that we don't know yet*:
    * share the discussion of relearn 2016 (said or unsaid thoughts)
    * to get material form /   to take forms <> to form takes

( what to do with relearn in the future => the proposal / isn't it the overall topic anyway? )
=== what are still elements after three days that we want to work on?

┏━┛┏━┛┏┏ ┛┏━ ┛┏━┛━┏┛  ┏━┛┏━┛┏━┃┃ ┃┏━┛┏━┃┃ ┃┏━┃┏━┃┏━ 
┏━┛┏━┛┃┃┃┃┃ ┃┃━━┃ ┃   ━━┃┏━┛┏┏┛┃ ┃┏━┛┏┏┛┏━┃┃ ┃┃ ┃┃ ┃
┛  ━━┛┛┛┛┛┛ ┛┛━━┛ ┛   ━━┛━━┛┛ ┛ ┛ ━━┛┛ ┛┛ ┛━━┛━━┛━━ 

-(feminist) serverhood (track?discussion).. things need to be done! this can be articulated with the "read the man in the man" section, but also with the multiple layered practices of networking. experiments about network architecture, engaging critically with tools, methods, practices
pick from the folders previously know as "the relearn archive" : DHCP cards (2015) -> specifics of the local server, a real recipe with "real" scripts, ssh tunnel, a feminist net/work how-to (http://relearn.be/2014/ )
https://gitlab.com/relearn/relearn2014/blob/master/Network2013.svg  https://gitlab.com/relearn/relearn2014/blob/master/Network2014.svg

━┏┛┛┏┏ ┏━┛
 ┃ ┃┃┃┃┏━┛
 ┛ ┛┛┛┛━━┛

-time (removal of year separation, but also tension between server-time and human-time, but also relearn-time and summerschool-time, calafou-time): objective/inscribed/notated temporalities as programming of slots etc, but also cultural and political implications of time-related decisions, but also subjective relationalities with time... / sensibility on time that is dedicated to... / and time dedicated to certain/specific sensibilities... / the issue of relearn as/not an isolated island (time,space,economy: privileges & their coordinates); relation to context (e.g. this week, calafou)->breaks the fiction of omnipresent totality (of people/past experiences etc) -> is this something that could be attended as an ephimeral situated knowledge? /


-read the man in the man, howtos, recipes, 10 things to be embarassed about... physical (fictionalised) terms of service, terms of use / heterotopia (https://en.wikipedia.org/wiki/Heterotopia_(space) ....BUT https://en.wikipedia.org/wiki/Gray_matter_heterotopia ) -> obstruction manual (relearn as an obstruction manual towards education? free software methodologies within education?)How to approach tools/recipes/language including the 'but'? how it relates to the feminist approach(es) of technologies? engaging with software as a cultural object that "do" things at different levels: software as executable, software as text, software as critique (https://radar.squat.net/en/event/amsterdam/lag/2016-02-14/software-critique ).
pick from the folders previously know as "the relearn archive": "merging" page on the relearn13 publication  http://relearn.be/2013/r/notes::merging.html -> discussions around, on, with tools and/or: method of Critical Fork http://relearn.be/2015/training-common-sense/sources/software/pattern-2.6-critical-fork/ , new padylon -> a way to talk about pads as tool, labyrinth http://relearn.be/2014/etherpad_archive/new_padylon.html
http://etherpad.calafou/p/relearn_manman


-reading/watching/discussion group on alternative education structures /critical pedagogy / selfmanaged learning situations, to articulate a bit the relation between relearn and those... irc watching/reading group? maybe a subtitle file to collect comments. pad-to-srt? who knows (perhaps using active archive videowiki http://activearchives.org/aaa/ ?)
list of materials from the learning tree nursery of relearn : http://relearn.be/2013/r/pedagogy::references.html    http://osp.constantvzw.org:9999/p/reroam



======================================================
just as a reminder:
    
day3? speculation:
-Time: Take away the years/editions as container. How to make context legible.e.g. where/when am i
-The psycho-research-unit archive: Improvising, voicing the archive, re-reading exercise. Creating new links-conections between materials from different times-contexts. The meta-pata-physics of archiving!
-Archival choices: what has been made accesible already and in what way. How it was decided of what to document? Difference in archive / documentation.
-Tagging exercises (bag of worms/can of words) 
-Issue of archival potential as something from which things can be learned: questioning the situation -> recipe -> situation paradigm.. (the e-learning utopia)
-Locating the cut-off point, scale: what reflections can, do happen on site, on the spot and where you need to create distance (space, time)
-The discourse around a collective event that usually happens in the periphery. Thinking of a form to make the discussion possible rather than starting it?

day3 again:
- an ssh tunnel
- an etherdump - Psychomagnotheric Slime? http://img4.wikia.nocookie.net/__cb20090716021023/ghostbusters/images/2/2d/Moodslime.png
- time/keeping server-service time
- physical (fictionalised) terms of service, terms of use / heterotopia`
- make the antenna more stable
- cron-job switching something (what) on/off
- a config-file
- a logic. trinary? BUT-gate

from day 2:
- DHCP cards (2015) -> specifics of the local server, a real recipe with "real" scripts
- install yourself into relearn -> 2014 card set (not on archive :/ ), introduction, merging levels, education as a topic to discuss
-> 2015 collective introduction / this one? -> http://pad.constantvzw.org/p/install_yourself_into_relearn // it departed from this other one -> http://pad.constantvzw.org/p/relearn_thinking_through
- new padylon -> a way to talk about pads as tool, labyrinth http://relearn.be/2014/etherpad_archive/new_padylon.html
- "merging" page on the relearn13 publication  http://relearn.be/2013/r/notes::merging.html -> discussions around, on, with tools and/or: method of Critical Fork http://relearn.be/2015/training-common-sense/sources/software/pattern-2.6-critical-fork/
- appendindex?-> continuity: http://relearn.be/2015/etherdump/continuity.html -> documentation, closing, sharing
- a track and content
- rotating practice

For exploring the archive:
obstructions? // https://en.wikipedia.org/wiki/The_Five_Obstructions "metaphors are obstructions"-->water flows analogy?http://relearn.be/2015/etherdump/trinary-logic.html // Kluge / ideology flows:  https://www.youtube.com/watch?v=BFXI1EBowpQ
- space (where,who, nomadism, dispersion)
- time (remove, extend into preparations, endurance., looping, repetition, iterations...)
- recipes (instructions to perform an action)
- cooking (an action wih an outcome, sometimes)

From the very libraries:

* http://www.open-frames.net/pdf/164-The_idea_of_critical_e-ducational_research.pdf
*Agamben's Profanations: chapter "
In Praise of Profanation" (pages 73-88)  http://www.elimeyerhoff.com/books/Agamben/Agamben%20-%20Profanations.pdf
-deschooling society, ivan illich

