DAY 2

List important element that were looked at yesterday:
- DHCP cards (2015) -> specifics of the local server, a real recipe with "real" scripts
- install yourself into relearn -> 2014 card set (not on archive :/ ), introduction, merging levels, education as a topic to discuss
-> 2015 collective introduction / this one? -> http://pad.constantvzw.org/p/install_yourself_into_relearn // it departed from this other one -> http://pad.constantvzw.org/p/relearn_thinking_through
- new padylon -> a way to talk about pads as tool, labyrinth http://relearn.be/2014/etherpad_archive/new_padylon.html
- "merging" page on the relearn13 publication  http://relearn.be/2013/r/notes::merging.html -> discussions around, on, with tools and/or: method of Critical Fork http://relearn.be/2015/training-common-sense/sources/software/pattern-2.6-critical-fork/
- appendindex?-> continuity: http://relearn.be/2015/etherdump/continuity.html -> documentation, closing, sharing
- a track and content
- rotating practice

For the presentation:
when?
where?
who?
why?
how?
what?
- coincidences and differences with calafou: pads uses:
that we don't need to blackbox certain problems because there is not pressure of creating a tool
**we don't read the manual to get it done necessarily, but read the manual to critique the writing 

Not just looking for a service, but for a relation
so where, how is Relearn activist/political - we can see in the archive, that there is a link between discursive and practical application, back and forth --> "software as a critique"
Recipes for situations that are equally productive and reflective = critical ? Research?

Recipes for critique
Executable fieldnotes
How do the productive and the reflective have a shared space-time? (maybe it is already happening on the level of individual technical objects, but harder on the level of Relearn as a project)
Dropping out and switching dyniamics is possible and desirable // tracks!
Who can and does participate? / figures, affordances, privileges, preasumption of knowledges and presences...
accessible/affordable
budget, thinking through power relations happens in preparations

through continuous use of pads, converging of preparations, event and afterlives.

when and how can reroam kinds of discussions happen? we tried peripheral times in different editions (lunch, breakfast...)


For exploring the archive:
obstructions? // https://en.wikipedia.org/wiki/The_Five_Obstructions "metaphors are obstructions"-->water flows analogy?http://relearn.be/2015/etherdump/trinary-logic.html // Kluge / ideology flows:  https://www.youtube.com/watch?v=BFXI1EBowpQ
- space (where,who, nomadism, dispersion)
- time (remove, extend into preparations, endurance., looping, repetition, iterations...)
- recipes (instructions to perform an action)
- cooking (an action wih an outcome, sometimes)


Anarcha server presentation
- physicalisation of the anarcha server at Calafou : rehabilitation of a room dedicated to anarcha ( https://anarchagland.hotglue.me/ )
interesting move, shifting from the usual critical-network-narrative in which you should move all the services in your house (homeservers, etc). Instead in this case virtualization is accepted in trusted places, and the physicalization happens with a physical room. the room uses many metaphors and analogies (ie entrance via port22, presence of physical mirrors) and is used as a catalyst to meet people and involve them further in the project.
(for instance need to inscribe your login to get in, "nekrocementery to host feminist websites that are no longer online)
3 'virtual machines', one for nekro-data, two living data (blog, ), three transitiory data ( host files, ), "mediagoblin" (sp?) (zombie data : images and audio to be reused in some way or another)
- need of desire creation to know/understand the whys and hows, to provoke the valuing of autonomous servers in (feminist) communities (<- is this desire creation the definition of pedagogy?)
- current struggle is mostly to reach a resilient economy/ecology for the people is busy with the project, and start providing services to activist communities.



