mplayer -vf mirror -v tv:// -tv device=/dev/video0:driver=v4l2

re-enactment in an online chat room of excerpt from the IRC conversation between bradass87 & info@adrianlamo.com in 2010 :
bradass87: hi
bradass87: how are you?
bradass87: im an army intelligence analyst, deployed to eastern baghdad, pending discharge for "adjustment disorder" in lieu of "gender identity disorder"
bradass87: im sure you’re pretty busy…
bradass87: if you had unprecedented access to classified networks 14 hours a day 7 days a week for 8+ months, what would you do?
info@adrianlamo.com <AUTO-REPLY>: Tired of being tired
bradass87: ?
bradass87: and you saw incredible things, awful things ... things that belonged in the public domain, and not on some server stored in a dark room in Washington DC ... 
bradass87: say ... a database of half a million events during the iraq war ... from 2004 to 2009 ... with reports, date time groups, lat-lon locations, casualty figures ...? or 260,000 state department cables from embassies and consulates all over the world, explaining how the first world exploits the third, in detail, from an internal perspective? 
bradass87: lets just say *someone* i know intimately well, has been penetrating US classified networks, mining data like the ones described ... and been transferring that data from the classified networks over the “air gap” onto a commercial network computer ... sorting the data, compressing it, encrypting it, and uploading it to a crazy white haired aussie who can't seem to stay in one country very long ...
bradass87: crazy white haired dude = Julian Assange
bradass87: and ... its important that it gets out ... i feel, for some bizarre reason. it might actually change something
bradass87: i just ... dont wish to be a part of it ... at least not now ... im not ready ... i wouldn't mind going to prison for the rest of my life, or being executed so much, if it wasn't for the possibility of having pictures of me ... plastered all over the world press ... as [a] boy ...
bradass87: i've totally lost my mind ... i make no sense ... the CPU is not made for this motherboard ... [...]
bradass87: i cant believe what im confessing to you :’(
bradass87: so ... it was a massive data spillage ... facilitated by numerous factors ... both physically, technically, and culturally
bradass87: listened and lip-synced to Lady Gaga's Telephone while exfiltratrating possibly the largest data spillage in american history [...]
bradass87: weak servers, weak logging, weak physical security, weak counter-intelligence, inattentive signal analysis ... a perfect storm [...]
bradass87: i mean what if i were someone more malicious
bradass87: i could've sold to russia or china, and made bank?
info@adrianlamo.com: why didn't you?
bradass87: because it's public data [...]
bradass87: it belongs in the public domain
bradass87: Information should be free

echo $(( ( $(date -ud '2016-08-30' +'%s') - $(date -ud '2015-02-11' +'%s') )/60/60/24 )) days

wget -qO - -i urls.txt | egrep -A 1 '(<title>|Limited script-free view:)'

man wget

surfing/clicking around websites on the spot :
https://wardiaries.wikileaks.org/
https://twitter.com/xychelsea
https://www.chelseamanning.org/




