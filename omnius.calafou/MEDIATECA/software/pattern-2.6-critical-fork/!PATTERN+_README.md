Pattern+
=====

Pattern+ is an annotated edition of the webmining module Pattern. The alpha-release of Pattern+ serves two interconnected goals: It is first of all an attempt to grow our understanding of - and concern with - the culture of data-mining, and then to feed this back into the practice of data-mining itself. It is also an experiment with forms of collaborative techno-critique that locate themselves closely to actual software objects.

What kind of assumptions do we encounter when valuating language from the point of view of an algorhythm? How do difference, ambiguity and dissent function in the context of so-called 'knowledge discovery'? By exploring the actual math and methods converging in data-mining, by close-reading code, algorithms and attentively documenting these experiments, Pattern+ offers entry points for understanding what agency human and computational actors might have in the co-production of 'common sense'.

Pattern+ could be of use for anyone interested in a contextual and critical understanding of data-mining, its methods and promises. Its various elements were developed by artists, developers, statisticians and designers during Cqrrelations http://cqrrelations.constantvzw.org/, in the context of a seminar on Algorithmic Cultures http://www.bak-utrecht.nl/en/Program/PosthumanGlossary/AlgorithmicCultures and at Relearn 2015 http://relearn.be 

What is Pattern anyway?
-------
Pattern is a Python software module aimed at data-mining of sources available on the web. It is a collection of scripts, algorithms, corpora, lexicons and examples being developed and added to by the research center CLiPS, associated with the Linguistics department of the faculty of Arts of the University of Antwerp. We chose Pattern as a starting point since it covers most of the operations involved in current data-mining practices (from ingestion to tokenizaton and vector-mapping), making use of many of the usual algorithms and methods. The use of Python as a programming language and the particularly legible set-up of the package make it useful for novice users, but because Pattern is also being put to work for large-scale research projects such as Amica, we think it makes sense to take it seriously as a professional environment. Future work will branch out to other Open Source packages such as Weka and ...

#!PATTERN+
------
Pattern itself is released under a BSD license, a permissive license that allows all users to run, study, modify, and distribute source code for any purpose. Potentially this establishes a space for interaction and discussion, an invitation to challenge the assumptions of the technological objects that are released under such a license. To explore this potential, we decided to imagine Pattern+ as a 'fork' of the original package, and to use the format of a 'pull request' as a way of publishing the project.

We used software-inspired methods for commenting and annotations. Some of these formats we decided to reformulate or re-imagine, as our interrogations did not always fit the usual templates. 

Annotations happen on several levels, both literally and figuratively. Pattern+ consists of:

* Annotated example files
* Code comments
* Comment files
* Tutorials
* Examples
* Suggestions, questions to invite further work
* Additional readings
* ...

Comments are all marked with #!PATTERN+ which might make automatic extraction and indexing possible in the future.

Contributors
--------
Manetta Berends, Christina Colchoir, Frederic Janssens, Anne Laforet, My Lê, Femke Snelting, Kym Ward

If you want to get involved in future work, find us on the Cqrrelations mailinglist https://lists.constantvzw.org/listinfo/cqrrelations

License
---------
Free Art License #FIXME: Check compatibility with BSD (Pattern)


