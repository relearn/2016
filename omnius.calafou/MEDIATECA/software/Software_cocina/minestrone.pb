// Relearn Minestrone Engine, Calafou Flavor!
// A collective recipe cooked during the 2014 and 2015 encounters.
// A minestrone of text-generation and convivial moments.
//
// To be run with Dada Engine ( http://dev.null.org/dadaengine/ )

#include <stdmap.pbi>
#include <format.pbi>    

minestrone: intro contents method how-1 how-2 how-3 how-4 imperative ;

intro: " MINESTRONE is a traditional Roman soup that consists mostly of vegetables, such as: " ingredient-1 ", " ingredient-2 ", "  ingredient-3 ", "  ingredient-4 ", "  ingredient-5 ", "  ingredient-6 ", " ingredient-7 ", "  ingredient-8 " and "  ingredient-9 ". This version is a bit simpler, but basically follows the same method and combination of ingredients.\n"
;
ingredient-1: "a network with an attitude" | "anarchaserver"
;
ingredient-2: "IP addresses" | "coffee" | "serigrafia"| "spirulina"
;
ingredient-3: "assembleas" | "washed and sliced scripts"
;
ingredient-4: "meta-antennas" | "dada engines" | "unfinished projects"
;
ingredient-5:  "noise" | "bread"
;
ingredient-6: "compost-chickens" | "ethernet cables" | "lentils"
;
ingredient-7: "failures" | "party"
;
ingredient-8: "minestrone pattern" | "raw data"
;
ingredient-9: "learning situations" | "mosquitoes"
;

contents: "\n\nINGREDIENTS: anarchaserver, parsley, carrots, pocket servers, bread, IP addresses, coffee, BUT gate, scripts, meta-antennas, spirulina, fractals, bags of chopped words, pads, mixed herbs, ethernet cables, green parmiggiano, failures, celery, party, portleaves, cal tab, raw data, learning situations, mosquitoes, 50 spoons.\n" ;

method: "\n\nMETHOD:\n" ;

how-1: "\n10:00 Step 1: Saute chopped " noun-1 ", diced " noun-2 " and " noun-3 " in olive oil until lightly colored. As soon as done: add diced fresh " noun-4 ". \n" ;

noun-1: "a network with an attitude" | "anarchaserver"
;
noun-2: "IP addresses" | "coffee" | "serigrafia" | "spirulina"
;
noun-3: "anarcha gland" | "scripts"
;
noun-4: "meta-antennas" | "dada engines" | "unfinished projects"
;

how-2: "\n10:30 Step 2: Fry " noun-5 " and " noun-6 " until they start to dissolve. Add tinned " noun-7 " and half of the chopped parsley.\n" ;

noun-5: "rinsed fractals" | "noise" | "bread"
;
noun-6: "compost-chickens" | "ethernet cables" | "lentils"
;
noun-7: "failures" | "pads" | "party"
;
noun-8: "learning situations" | "mosquitoes"
;
how-3: "\n11:00 Step 3: Check that the soup is boiling softly. Stir gently, add 3 portleaves and a cup of water from the toxic river.\n"
; 
how-4: "\n12:00 Step 4: Add " noun-8 " and stir playfully.\n" ; 

imperative: " \n13:30 : " sentence ;

sentence : "LUNCH READY!" | "BON PROFIT!" | "INSTALL YOURSELVES INTO RELEARN!"  
        |  "MINE THE STRONE!" | "EVENTUALLY GOOD APPETITE!"
;








