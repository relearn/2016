Setup Network Nodes
=====================

Based on [OpenWRT Routed AP Recipe](http://wiki.openwrt.org/doc/recipes/routedap)


/etc/config/network
--------------------

This part can stay the same.

        config interface 'loopback'
                option ifname 'lo'
                option proto 'static'
                option ipaddr '127.0.0.1'
                option netmask '255.0.0.0'

ALTER

        config interface 'lan'
                option ifname 'eth0'
                # option type 'bridge'
                option proto 'dhcp'  # from 'static'
                # option ipaddr '192.168.1.1'
                # option netmask '255.255.255.0'

ADD

        config 'interface' 'wifi'
                option 'proto'          'static'
                option 'ipaddr'         '192.168.XXX.1'
                option 'netmask'        '255.255.255.0'


where XXX is the number of the subnet.


/etc/config/wireless
----------------------

Change "lan" to "wifi", and comment out the disabled command. Set the ssid (and eventually security / password).

        config wifi-iface
                option device   radio0
                option network  wifi
                option mode     ap
                option ssid     botnet-cindi
                option encryption none

/etc/config/dhcp
--------------------

Enable dhcp on the wifi interface.

ADD TO END:

        config dhcp 'wifi'                          
                option interface 'wifi'             
                option start 100                  
                option limit 150                  
                option leasetime 12h  



/etc/config/firewall
-----------------------------

Add the masq option to the existing lan section:

        config zone                                            
                option name             lan                    
                option network          'lan'                  
                option input            ACCEPT                 
                option output           ACCEPT                 
                option forward          REJECT                 
                option masq             '1'                    # LINE 15

And add to the end a new zone for the wifi + rules that connect it.

        config zone                                            
                option  name    wifi                           
                list    network 'wifi'                         
                option  input   ACCEPT                         
                option  output  ACCEPT                         
                option  forward REJECT                         
                                                               
        config 'forwarding'                                    
                option 'src'    'wifi'                         
                option 'dest'   'wan'                          
                                                               
        config 'forwarding'                                    
                option 'src'    'lan'                          
                option 'dest'   'wifi'                         
                                                               
        config 'forwarding'
                option 'src'    'wifi'                         
                option 'dest'   'lan'                          

reboot
--------
reboot -f
